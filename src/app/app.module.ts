import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireStorageModule } from 'angularfire2/storage'
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { LoginComponent } from './auth/login/login.component';
import { FileUploaderComponent } from './file-uploader/file-uploader.component';
import { RegisterComponent } from './auth/register/register.component';
import { AuthService } from './auth/auth.service';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { environment} from '../environments/environment';
import { MatProgressBarModule, MatButtonModule, MatFormFieldModule, MatInputModule, MatRippleModule} from '@angular/material';
import { FileSizePipe } from './helpers/pipes/file-size.pipe'

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    FileUploaderComponent,
    RegisterComponent,
    FileSizePipe,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.config, 'users-picture'),
    AngularFireAuthModule,
    AppRoutingModule,
    NgbModule,
    AngularFireStorageModule,
    AngularFireDatabaseModule,
    MatProgressBarModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
