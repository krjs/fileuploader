import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormControl, Validators, AbstractControl, ValidationErrors } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  regForm: FormGroup;

  constructor(private authServie: AuthService ) { }

  ngOnInit() {
    this.regForm = this.setNewRegisterForm();
  }

  register() {
    let dataVal = {
      name: <string>this.regForm.get('name').value,
      email: <string>this.regForm.get('email').value,
      password: <string>this.regForm.get('password').value
    }
    this.authServie.register(dataVal.name, dataVal.email, dataVal.password)
  }

  setNewRegisterForm() {
    return new FormGroup({
      name: new FormControl(null, [Validators.required, Validators.minLength(3)]),
      email: new FormControl(null, [Validators.required, Validators.minLength(3), Validators.email]),
      password: new FormControl(null, [Validators.required, Validators.minLength(6)]),
    })
  }
}
