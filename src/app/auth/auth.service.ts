import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from 'firebase';
import { Router } from '@angular/router';
import { AngularFireDatabase } from 'angularfire2/database';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: User;
  usersConfigList;
  private logInErrorSubject = new Subject<string>();
  constructor(private angularFire: AngularFireAuth, private router: Router, public angularFireDb: AngularFireDatabase ) {
    angularFire.authState.subscribe(user => {
      this.user = user;
    })
  }

  public getLoginErrors():Subject<string>{
    return this.logInErrorSubject;
}

  login(email: string, password: string) {
    this.angularFire.auth.signInWithEmailAndPassword(email, password).then(usera => {
      this.router.navigate(['/file-upload']);
    }).catch(err => {
      this.logInErrorSubject.next(err.message);
    })
  }

  logout() {
    this.angularFire.auth.signOut();
  }

  register(name: string, email: string, password: string) {
    this.angularFire.auth.createUserWithEmailAndPassword(email, password).then(user => {
      this.storeUserData(user.user.uid, name)
      this.router.navigate(['/file-upload']);
    }).catch(err => {
      console.log(err)
    })
  }

  storeUserData(userID, name) {
    const userData = {
      user_name: name
    }

    this.usersConfigList = this.angularFireDb.list('/users_data/' + userID)
    this.usersConfigList.push(userData)
  }
}
