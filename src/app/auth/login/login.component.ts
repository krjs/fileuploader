import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { NgForm } from '@angular/forms';
import { FormGroup, FormArray, FormControl, Validators, AbstractControl, ValidationErrors } from '@angular/forms';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public errorMessage: string;
  constructor(private authServie: AuthService) { }

  ngOnInit() {
    this.authServie.getLoginErrors().subscribe(error => {
      this.errorMessage = error;
    });
  }

  login(loginFormData: NgForm ) {
    this.authServie.login(loginFormData.value.email, loginFormData.value.password)
  }
}
