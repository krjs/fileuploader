import { AuthService } from './../auth/auth.service';
import { Component, OnInit } from '@angular/core';
import { AngularFireStorage } from 'angularfire2/storage';
import { AngularFireDatabase } from 'angularfire2/database';
import { MatProgressBarModule } from '@angular/material'
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-file-uploader',
  templateUrl: './file-uploader.component.html',
  styleUrls: ['./file-uploader.component.scss']
})
export class FileUploaderComponent implements OnInit {
  selectedFiles: FileList;
  file: File;
  textMe: any;
  ref: any;
  public user: any;
  checkData;
  items: any;
  userId: string;
  hideElem:boolean;
  uploadProgress: Observable<number>;
  constructor(private storage: AngularFireStorage, public angularFireDb: AngularFireDatabase, private authService: AuthService) {
    this.userId = this.authService.user.uid;
    this.user = this.angularFireDb.list('/users/' + this.userId)
    this.hideElem = true;
  }

  ngOnInit() {
    this.userId = this.authService.user.uid;
    this.user = this.angularFireDb.list('/users/' + this.userId)
    this.items = this.angularFireDb.list('/users/' + this.userId, ref => ref.orderByChild('file_name')).valueChanges();
    this.items.subscribe(valueOfItems => {
      this.checkData = valueOfItems
    })
  }


  upload(event) {
    let fileToSave = event.target.files[0];
    let newData = {
      file_id: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15),
      file_name: this.getFileNameWithoutExtension(fileToSave.name),
      file_type: fileToSave.type,
      file_size: fileToSave.size,
      file_link: ''
    }

    this.storage.upload('/upload/' + this.userId + '/' + newData.file_id, event.target.files[0]).then(value => {
      let url = value.ref.getDownloadURL();
      url.then(value => {
        newData.file_link = this.hideUrl(value);
        this.user.push(newData);
      })
    });
  }

  getFileNameWithoutExtension(name: string) {
    return name.substring(0, name.lastIndexOf('.'));
  }

  hideUrl(url: string) {
    return window.btoa(url)
  }

  showUrl(url:string) {
    return window.atob(url)
  }

  editItem() {
    this.hideElem = false;
  }

  downloadItem(id: string) {
    let checkUrl = this.angularFireDb.list('/users/' + this.userId , ref => ref.orderByChild('file_id').equalTo(id)).valueChanges();

    checkUrl.subscribe(val => {
      val.map(item =>{
        let url = this.showUrl(item['file_link']);
        window.open(url, 'Download');
      })
    })
  }

  deleteItem(id: string) {
    let x = this.angularFireDb.list('/users/' + this.userId, ref => ref.orderByChild('file_id').equalTo(id));
    x.snapshotChanges().subscribe(item =>{

      item.forEach(value =>{
        let urlToDel = this.showUrl(value.payload.val()['file_link'])
        this.storage.storage.refFromURL(urlToDel).delete();
        x.remove(value['key'])
      })
    })
  }

  cancelAction() {
    this.hideElem = true;
  }

  editSelectedItem(id:string, text) {
    let inputValue: HTMLInputElement =<HTMLInputElement>document.getElementById(text)
    let x = this.angularFireDb.list('/users/' + this.userId, ref => ref.orderByChild('file_id').equalTo(id));
    x.snapshotChanges().subscribe(item =>{
      item.forEach(value =>{
        x.update(value['key'], {file_name: inputValue.value})
      })
    })

    this.cancelAction();
  }
}
